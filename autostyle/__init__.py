# Copyright (C) 2014  Ben Ockmore
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses

"""Package initialization file, for registering blueprints within
modules in a create_app function. Also sets up SQLAlchemy.
"""


from flask import Flask
from schema import db
from proposal import proposal_pages

def create_app():
    app = Flask(__name__)
    app.register_blueprint(proposal_pages, url_prefix='/proposal')
    app.config.update({
        'SQLALCHEMY_DATABASE_URI':'sqlite:///temp.db'
    })
    
    db.app = app
    db.init_app(app)

    return app
