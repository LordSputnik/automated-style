# Copyright (C) 2014  Ben Ockmore
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses

"""Module containing flask endpoints for everything to do with style
proposals.
"""

from flask import Blueprint, render_template

proposal_pages = Blueprint('proposal_pages', __name__,
                           template_folder='templates')

@proposal_pages.route('/<id>')
def proposal_get(id):
    return render_template('proposal.html')
