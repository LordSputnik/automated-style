from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Editor(db.Model):
    __tablename__ = 'editor'
    
    id = db.Column(db.Integer, primary_key=True)
    
    username = db.Column(db.UnicodeText)
    
    email = db.Column(db.UnicodeText)


class Proposal(db.Model):
    __tablename__ = 'proposal'
    
    id = db.Column(db.Integer, primary_key=True)
    
    title = db.Column(db.UnicodeText)
    
    text = db.Column(db.UnicodeText)
    
    start = db.Column(db.DateTime)
    finish = db.Column(db.DateTime)

    ended = db.Column(db.Boolean)
    accepted = db.Column(db.Boolean)
    
    proposer = db.Column(db.Integer, db.ForeignKey('editor.id'))



class Comment(db.Model):
    __tablename__ = 'comment'
    
    id = db.Column(db.Integer, primary_key=True)
    
    proposal = db.Column(db.Integer, db.ForeignKey('proposal.id'))
    
    author = db.Column(db.Integer, db.ForeignKey('editor.id'))

    timestamp = db.Column(db.DateTime)
    
class Vote(db.Model):
    __tablename__ = 'vote'
    
    id = db.Column(db.Integer, primary_key=True)
    
    voter = db.Column(db.Integer, db.ForeignKey('editor.id'))
    
    proposal = db.Column(db.Integer, db.ForeignKey('proposal.id'))
    
    direction = db.Column(db.Integer)
